numpy
pandas
colorlog
pyarrow
tensorflow==2.0.0-beta1
scikit-learn
matplotlib
geopy
IPython
influxdb
