from .SOAIOpenAirSensor import SOAIOpenAirSensor
from .SOAILanuvSensor import SOAILanuvSensor

import pandas as pd
import logging
import numpy as np
import os
logger = logging.getLogger()

## Class which represents a sensor network
#
# It takes care of loading all the sensor from som configuration file and manages the communication between them and to the outside.
class SOAISensorNetwork():

    # This list correspond to the type of sensors which are available in this network. Other types of sensors in the configuration file will be skipped.
    sensorTypes = ["OpenAirCologne", "Lanuv"]

    ## Initializes the network with sensors from the configuration file.
    #
    # The configuration file needs to be written in a very specific form. The columns are seperated by whitespaces and in the following order:
    #   Sensor type | ID | Laitude | Longitude | Active | Sensor calibration (optional)
    #
    # Valid values are:
    #   Sensor type: OpenAirCologne, Lanuv
    #   ID: String containing the ID
    #   Latitude: Value of latitude
    #   Longitude: Value of longitude
    #   Active: 1 for active or 0 for inactive
    #   Sensor calibration: Path to the calibration model for OpenAirCologn sensors
    #
    # @param pathToConfigFile Path and filename of the configuration file
    def __init__(self, pathToConfigFile):
        self.listSensors = []

        with open(pathToConfigFile) as f:
            for line in f:
                configs = line.rstrip('\n').split(" ")
                if len(configs) < 4:
                    logger.error(f"The following line can not be used to set up a sensor: {line}")
                    continue

                sensorType = configs[0]
                sensorID = configs[1]
                sensorLocation = (float(configs[2]),float(configs[3]))
                active = int(configs[4])

                if sensorType == "OpenAirCologne":
                    sensorTEMP = SOAIOpenAirSensor(sensorID, sensorLocation)

                    # By default the sensors are not active
                    if active == 1:
                        sensorTEMP.fSetActive()

                    # If a path is available from the config file set the calibration model
                    if len(configs) > 5:
                        pathModel = os.environ.get("SOAI") + "/" + configs[5] + "/" + sensorID + ".h5"
                        pathScaler = os.environ.get("SOAI") + "/" + configs[5] + "/" + sensorID + "_scaler.sav"
                        sensorTEMP.fSetCalibration(pathModel, pathScaler)

                    self.listSensors.append(sensorTEMP)

                elif sensorType == "Lanuv":
                    sensorTEMP = SOAILanuvSensor(sensorID, sensorLocation)
                    if active == 1:
                        sensorTEMP.fSetActive()
                    self.listSensors.append(sensorTEMP)

                else:
                    logger.error(f"Sensor type is not known. Skip line {line}.")

    def fCheckSensorType(self, sensorType):
        if sensorType not in self.sensorTypes:
            return False
        else:
            return True

    ## Checks if the sensors are set up correctly
    def fCheckNetwork(self):

        for sensorType in self.sensorTypes:
            countActive = 0
            countSum = 0
            countWronglyActive = 0

            for sensor in self.listSensors:

                if sensor.fGetType() == sensorType:
                    countSum += 1

                    if sensor.fIsActive() is True:
                        countActive += 1

                        if sensorType == "OpenAirCologne" and sensor.fHasCalibration() is False:
                            countWronglyActive += 1
                            logger.error(f"OpenAirCologne sensor {sensor.fGetID()} is active but does not have a calibration model set. Set as inactive.")
                            sensor.fSetInactive()

            logger.info(f"Status of {sensorType} sensors:\n\t{countActive} of {countSum} are active.\n\t{countWronglyActive} of these have an error.")

    def fFindSensor(self, sensorType, ID):
        if self.fCheckSensorType(sensorType) is False:
            logger.error(f"Sensor type {sensorType} is not known.")
            raise Exception(f"Sensor type {sensorType} is not known.")

        for sensor in self.listSensors:
            if sensor.fGetType() == sensorType and sensor.fGetID() == ID:
                return sensor

        logger.warning(f"Sensor of type {sensorType} with ID {ID} not found in network.")
        return None

    ## Gets a data frame with measurments and converts the measurments to NO2 values
    #
    # @param data Pandas data frame with at least the columns OpenAirCologne: ID (feed) | r2 | temp | hum; Lanuv: no2
    # @returns dataNO2 Same data frame as data but with an additional column no2 for the NO2 values
    def fDataToNO2(self, data, sensorType):
        if self.fCheckSensorType(sensorType) is False:
            logger.error(f"Sensor type {sensorType} is not known.")
            raise Exception(f"Sensor type {sensorType} is not known.")

        if isinstance(data, pd.DataFrame) is False:
            logger.error("Data has to be give as a pandas data frame.")
            raise Exception("Data has to be give as a pandas data frame.")

        # Check for necessary columns
        neededColumns = []
        idName = ""
        if sensorType == "OpenAirCologne":
            neededColumns = ["feed", "r2", "temp", "hum"]
            idName = "feed"
        elif sensorType == "Lanuv":
            neededColumns = ["no2"]
            idName = "station"

        columns = data.columns
        for col in neededColumns:
            if col not in columns:
                logger.error(f"Data has no column {col}.")
                raise Exception(f"Data has no column {col}.")


        # Convert the data to NO2 measurments using the sensor network
        no2 = np.ones((len(data)))*(-1) # Default values which are filled in the next for loop
        for ID in data[idName].unique():
            dataTEMP = data[data[idName] == ID]
            sensor = self.fFindSensor(sensorType, ID)
            no2TEMP = np.ones((len(dataTEMP)))*(-1)

            if sensor is None:
                logger.info(f"Skip non-existing {sensorType} sensor with ID {ID}")
            else:
                if sensor.fIsActive() is False:
                    logger.info(f"Skip unactive {sensorType} sensor with ID {ID}")
                else:
                    # Iterate over data frame with rows as dicts
                    countTEMP = 0
                    for k, row in dataTEMP.iterrows():
                        no2TEMP[countTEMP] = sensor.fDataToNO2(row)
                        countTEMP += 1

                no2[dataTEMP.index] = no2TEMP

        data["no2"] = no2

        return data
